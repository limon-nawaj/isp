Rails.application.routes.draw do

  get 'notification/index'
  get 'report/pending_service'
  get 'report/pending_bill'
  get 'report/service_catagory'

  resources :operators

  resources :icares do
    member do
      patch :assign_task
      # get   :operators
      patch :operator
    end
    collection do
      get :admin_index
      # get :services
    end
  end

  # match "/operators", to: "icares#operators", via: [:get]

  resources :icare_types

  resources :salary_invoices

  resources :salaries do
    member do
      patch :custom_paid
    end
  end

  resources :wallets

  resources :invoices

  resources :bills do
    member do
      patch :custom_paid
    end
  end

  resources :customers do
    member do
      post :active
      get :basic_info
      get :address
      get :login_info
    end
  end

  resources :packages

  resources :employees

  resources :roles

  resources :zones

  resources :organizations do
    resources :connections
    member do
      patch :logo
    end
  end

  # resources :user_organizations
  
  resources :districts do
    get "delete"
    resources :areas
  end

  resources :sessions, only: [:new, :create, :destroy]

  get 'admins' => 'users#admin'

  # get 'users/index'

  match '/singup',     to: 'users#new',              via: 'get'
  match '/signin',     to: 'sessions#new',           via: 'get'
  match '/signout',    to: 'sessions#destroy',       via: 'delete'

  resources :users do
    member do
      patch :upload
      patch :customer_upload
      patch :employee_upload
    end
  end

  root "home#index"

  get 'dashboard'=>'home#dashboard'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end