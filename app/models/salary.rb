class Salary < ActiveRecord::Base
	belongs_to :organization
	belongs_to :employee

	def self.search(date, batch_number)
		if date.present? && batch_number.present?
			employee = Employee.find_by_batch_number("#{batch_number}".to_i)
			where("strftime('%m%Y', month_year) = ? AND employee_id = ?", "#{date}", "#{employee.id}")
		elsif date.present?
			where("strftime('%m%Y', month_year) = ?", "#{date}")
		else
			all
		end
	end
end
