class Bill < ActiveRecord::Base
	belongs_to :organization
	belongs_to :customer

	def self.search(date, batch_number, package_id, zone_id)
		
		if date.present? && batch_number.present? && package_id.present? && zone_id.present?
			customer = Customer.find_by_batch_number("#{batch_number}".to_i)
			if customer && package_id && zone_id
				where("strftime('%m%Y', month_year) = ? AND customer_id = ? AND package_id = ? AND zone_id = ?", "#{date}", "#{customer.id}", "#{package_id}", "#{zone_id}")
			end	
		elsif date.present? && batch_number.present? && package_id.present?
			customer = Customer.find_by_batch_number("#{batch_number}".to_i)
			if customer && package_id
				where("strftime('%m%Y', month_year) = ? AND customer_id = ? AND package_id = ?", "#{date}", "#{customer.id}", "#{package_id}")
			end
		elsif date.present? && batch_number.present? && zone_id.present?
			customer = Customer.find_by_batch_number("#{batch_number}".to_i)
			if customer && zone_id
				where("strftime('%m%Y', month_year) = ? AND customer_id = ? AND zone_id = ?", "#{date}", "#{customer.id}", "#{zone_id}")
			end
		elsif date.present? && package_id.present? && zone_id.present?	
			if package_id && zone_id
				where("strftime('%m%Y', month_year) = ? AND package_id = ? AND zone_id = ? ", "#{date}", "#{package_id}", "#{zone_id}")
			end
		elsif date.present? && package_id.present?
			if package_id
				where("strftime('%m%Y', month_year) = ? AND package_id = ?", "#{date}", "#{package_id}")
			end
		elsif date.present? && zone_id.present?
			if zone_id
				where("strftime('%m%Y', month_year) = ? AND zone_id = ?", "#{date}", "#{zone_id}")
			end
		elsif date.present? && batch_number.present?
			customer = Customer.find_by_batch_number("#{batch_number}".to_i)
			if customer
				where("strftime('%m%Y', month_year) = ? AND customer_id = ?", "#{date}", "#{customer.id}")	
			else
				all
			end		
		elsif date.present?
			where("strftime('%m%Y', month_year) = ?", "#{date}")
		else
			all
		end
	end

	def self.search_by_zone_and_month(zone, month_year)
		if zone && month_year
			where("strftime('%m%Y', month_year) = ? AND zone_id = ?", "#{month_year}", "#{zone}")
		elsif zone
			where("zone_id = ?", "#{month_year}", "#{zone}")
		elsif month_year
			where("strftime('%m%Y', month_year) = ?", "#{month_year}")	
		else
			all
		end
	end

	def self.search_by_zone(zone)
		if zone
			where(zone_id: zone)
		else
			all
		end
	end

	# def self.search_by_month(date)
	# 	if date
	# 		where("strftime('%m%Y', month_year) = ?", "#{date}")
	# 	else
	# 		all
	# 	end
	# end

	# def self.search_by_customer(customer_id)
	# 	if customer_id
	# 		Customer.where("id = ?", "#{customer_id}")
	# 	else
	# 		Customer.all
	# 	end
	# end

	# def self.search_by_package(package_id)
	# 	if package_id
	# 		Package.where("id = ?", "#{package_id}")
	# 	else
	# 		Package.all
	# 	end
	# end

	# def self.search_by_zone(zone_id)
	# 	if zone_id
	# 		Zone.where("id = ?", "#{zone_id}")
	# 	else
	# 		Zone.all
	# 	end
	# end
end
