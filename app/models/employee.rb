class Employee < ActiveRecord::Base
	has_one :user, as: :meta, dependent: :destroy
	belongs_to 	:organization
	has_many 	:salaries
	has_one 	:wallet, as: :fee, dependent: :destroy
	has_many 	:icares
	belongs_to 	:role
	has_many 	:operators

	validates :batch_number, numericality: { only_integer: true }, :uniqueness => {:scope => :organization_id,:case_sensitive => false}
	validates :address, presence: true
	validates :join_date, presence: true
	validates :basic_salary, presence: true, numericality: true

	validates :first_name, presence: true
	validates :email, presence: true
	validates :mobile, presence: true
	validates :role, presence: true

	accepts_nested_attributes_for :user
	accepts_nested_attributes_for :wallet

# 	belongs_to :boss, :class_name => "Employee"
#  	has_many :reports, :class_name => "Employee", :foreign_key => "boss_id"

	def full_name
		"#{self.first_name}" " " "#{self.last_name}"
	end

	def self.search(search)
	  if search
	    where('batch_number LIKE ? OR first_name LIKE ? OR last_name LIKE ? OR email LIKE ? OR (first_name || " " || last_name) LIKE ?', 
	    	"%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}")
	  else
	    all
	  end
	end

	def total
		self.basic_salary.to_f+self.additional_salary.to_f
	end

	def salary
	   "%.2f TK" % self.total
	end

	def is_monthly_salary_exist(monthly)
		@salaries = self.salaries

		if @salaries.nil?
			false
		else
			salary = self.salaries.where("strftime('%m%Y', month_year) = ?", "#{monthly}").first
			if salary.nil?
				false
			else
				true
			end
		end
	end
end

