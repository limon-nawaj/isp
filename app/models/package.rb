class Package < ActiveRecord::Base
	belongs_to :organization
	has_many :connections

	validates :name, presence: true
	validates_uniqueness_of :name, :scope => :organization_id, :case_sensitive=>false
	validates :price, presence: true, numericality: true
	validates :data_speed, presence: true

	def total
		(self.price || 0) +((self.vat || 0)*(self.price || 0))/100
	end

	def package_price
		"#{name}" "-" "#{f_price}"
	end

	def f_price
	   "%.2f TK" % self.total
	end
end
