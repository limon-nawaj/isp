class IcareType < ActiveRecord::Base
	belongs_to :organization
	has_many :icares

	validates :name, presence: true
	# validates :required_time, presence: true
end
