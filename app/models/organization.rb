class Organization < ActiveRecord::Base
	belongs_to :area
	belongs_to :district
	has_many :users
	has_many :zones
	has_many :roles
	has_many :employees
	has_many :packages
	has_many :customers
	has_many :bills
	has_many :customer_cares
	has_many :salaries
	has_many :icare_types
	has_many :icares
	has_many :connections

	# mount_uploader :image, ImageUploader

	accepts_nested_attributes_for :users

	validates :name, presence: true, :if => :name
	validates :mobile, presence: true, :if => :mobile
	validates :district_id, presence: true, :if => :district_id
	validates :area_id, presence: true, :if => :area_id
	validates :house, presence: true, :if => :house
	validates :block, presence: true, :if => :block
	validates :about, presence: true, :if => :about


	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, 
				uniqueness: { case_sensitive: false }


	def self.search(area)
		if area
			where("area_id LIKE ?", "%#{area}%")
		else
			all
		end
	end

end
