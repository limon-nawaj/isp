class Role < ActiveRecord::Base
	belongs_to :organization
	has_many :employees

	validates :name, presence: true
	validates_uniqueness_of :name, :scope => :organization_id, :case_sensitive=>false
end
