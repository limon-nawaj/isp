class User < ActiveRecord::Base
  
  belongs_to :organization
  belongs_to :meta, polymorphic: true

  has_secure_password

  before_save { self.username = username.downcase }
  before_create :create_remember_token
  validates :username, presence: true, :uniqueness => {:case_sensitive => false}

  # VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  # validates :email, format: { with: VALID_EMAIL_REGEX }
  validates :password, :presence     => true,
                     :confirmation => true,
                     :length       => { :minimum => 6 },
                     :if           => :password
  
  # mount_uploader :image, ImageUploader

  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  def is_super_admin
  	return true if self.role == "super_admin"
  end

  def is_admin
  	return true if self.role == "admin"
  end

  def is_customer
    return true if self.role == "customer"
  end

  def is_employee
    return true if self.role == "employee"
  end

  def self.search(search)
    if search
      where('first_name LIKE ?', "%#{search}%")
    else
      all
    end
  end

  private 
    def create_remember_token
      self.remember_token = User.encrypt(User.new_remember_token)
    end
end
