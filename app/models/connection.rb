class Connection < ActiveRecord::Base
	belongs_to :organization
	belongs_to :package
	belongs_to :zone

	validates :first_name, presence: true
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  	validates :email, format: { with: VALID_EMAIL_REGEX }
  	validates :mobile, presence: true
  	validates :package_id, presence: true
  	validates :zone_id, presence:true
  	validates :house, presence:true
end
