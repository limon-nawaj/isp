class Area < ActiveRecord::Base
	belongs_to :district
	has_many :organization
	validates :name, presence: true
end
