class District < ActiveRecord::Base
	has_many :areas
	validates :name, presence: true, uniqueness: { case_sensitive: false }
end
