class Customer < ActiveRecord::Base
	has_one :user, as: :meta, dependent:  :destroy
	has_one :wallet, as: :fee, dependent: :destroy
	
	belongs_to 	:organization
	belongs_to 	:zone
	belongs_to 	:package
	has_many 	:bills
	has_many 	:customer_cares
	has_many 	:icares
	has_many 	:operators
	

	accepts_nested_attributes_for :user
	accepts_nested_attributes_for :wallet

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  	validates :email, format: { with: VALID_EMAIL_REGEX }

	# validation
	validates :batch_number, numericality: { only_integer: true }, :uniqueness => {:scope => :organization_id,:case_sensitive => false}
	validates :house, presence: true
	validates :join_date, presence: true

	validates :first_name, presence: true
	validates :email, presence: true
	validates :mobile, presence: true
	validates :package_id, presence: true
	validates :zone_id, presence: true
	validates :mac_address, presence: true

	def full_name
		"#{self.first_name}" " " "#{self.last_name}"
	end
	
	
	def self.all_customer_search(search)
	  if search
	  	s = "%#{search}%"
	    where('batch_number LIKE ? OR first_name LIKE ? OR last_name LIKE ? OR email LIKE ? OR (first_name || " " || last_name) LIKE ?', 
	    	s, s, s, s, s)
	  else
	    all
	  end
	end

	def self.current_customer_search(search)
	  if search
	  	s = "%#{search}%"
	    where('batch_number LIKE ? OR first_name LIKE ? OR last_name LIKE ? OR email LIKE ? OR (first_name || " " || last_name) LIKE ?', 
	    	s, s, s, s, s).where(active: true)
	  else
	    where(active: true)
	  end
	end

	def self.x_customer_search(search)
	  if search
	  	s = "%#{search}%"
	    where('batch_number LIKE ? OR first_name LIKE ? OR last_name LIKE ? OR email LIKE ? OR (first_name || " " || last_name) LIKE ?', 
	    	s, s, s, s, s).where(active: false)
	  else
	    where(active: false)
	  end
	end

	def is_monthly_bill_exist(monthly)
		@bills = self.bills

		if @bills.nil?
			false
		else
			bill = self.bills.where("strftime('%m%Y', month_year) = ?", "#{monthly}").first
			if bill.nil?
				false
			else
				true
			end
		end
	end

	


end
