class Icare < ActiveRecord::Base
	belongs_to :organization
	belongs_to :icare_type
	belongs_to :customer
	belongs_to :employee
	has_many   :operators

	accepts_nested_attributes_for :operators

end
