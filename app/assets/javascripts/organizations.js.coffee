jQuery ->
  $('#organization_area_id').parent().hide()
  areas = $('#organization_area_id').html()
  $('#organization_district_id').change ->
    district = $('#organization_district_id :selected').text()
    escaped_district = district.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(areas).filter("optgroup[label='#{escaped_district}']").html()
    if options
      $('#organization_area_id').html(options)
      $('#organization_area_id').parent().show()
    else
      $('#organization_area_id').empty()
      $('#organization_area_id').parent().hide()