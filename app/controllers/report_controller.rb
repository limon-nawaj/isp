class ReportController < ApplicationController
	layout "dashboard"
	def pending_service
		if params[:start_date].present? && params[:end_date].present?
			start_hash = params[:start_date]
			@start 	= Date.new(start_hash['(1i)'].to_i, start_hash['(2i)'].to_i, start_hash['(3i)'].to_i)

			end_hash = params[:end_date]
			@end 	= Date.new(end_hash['(1i)'].to_i, end_hash['(2i)'].to_i, end_hash['(3i)'].to_i)

			@report = current_organization.icares.where("created_at <= ?", 2.week.ago).where.not(status: 10).
				where(created_at: @start..@end).order(created_at: :desc).paginate(page: params[:page], per_page: 40)
		else
			@report = current_organization.icares.where("created_at <= ?", 2.week.ago).
				where.not(status: 10).order(created_at: :desc).paginate(page: params[:page], per_page: 40)
		end
	end

	def pending_bill
		@report = current_organization.bills.search_by_zone(params[:zone]).where("created_at < ? ", Date.today.at_beginning_of_month).paginate(page: params[:page], per_page: 40)	
		# if !params[:month_year].nil?
		# 	hash 	= params[:month_year]
		# 	@d 		= Date.new(hash['(1i)'].to_i, hash['(2i)'].to_i, hash['(3i)'].to_i)
		# 	@date 	= Date.parse(@d.to_s)
		# 	@my 	= Date.parse(@d.to_s).strftime("%m%Y")

		# 	@report = current_organization.bills.search_by_zone_and_month(params[:zone], @my).paginate(page: params[:page], per_page: 40)	
		# else
		# 	@report = current_organization.bills.search_by_zone(params[:zone]).paginate(page: params[:page], per_page: 40)	
		# end
	end

	def service_catagory
		if params[:start_date].present? && params[:end_date].present?
			start_hash = params[:start_date]
			@start 	= Date.new(start_hash['(1i)'].to_i, start_hash['(2i)'].to_i, start_hash['(3i)'].to_i)

			end_hash = params[:end_date]
			@end 	= Date.new(end_hash['(1i)'].to_i, end_hash['(2i)'].to_i, end_hash['(3i)'].to_i)
		end

		@icare_types = current_organization.icare_types
	end
end