class EmployeesController < ApplicationController
	layout "dashboard"
	before_action :set_employee, only: [:edit, :update, :show]
	before_action :authenticate_user!
	before_action :check_customer
	before_action :set_role

	def index
		@employees = current_organization.employees.search(params[:search])
	end

	def new
		@employee = Employee.new
		@employee.build_user unless @employee.user
		@employee.build_wallet unless @employee.wallet
	end

	def create
		@employee = Employee.new(employee_params)
		if @employee.save
			redirect_to employees_path
		else
			render 'new'
		end
	end

	def edit
	end

	def show
	end

	def update
     	if @employee.update_attributes(employee_params)
            flash[:notice] = "Employee updated successfully."
            redirect_to edit_employee_path(@employee)
        else
            render :action => 'edit'
        end
	end

	private 
		def employee_params
			params.require(:employee).permit(:first_name, :last_name, :mobile, :email, :organization_id, 
				:batch_number, :address, :basic_salary, :additional_salary, :join_date, :role_id, 
				wallet_attributes: [:amount], user_attributes: [:id, :organization_id, :username, :password, :password_confirmation, :role])
		end

		def update_employee_params
			params.require(:employee).permit(:first_name, :last_name, :mobile, :email, :organization_id, :batch_number, :address, :basic_salary, :additional_salary, :join_date)
		end

		def set_employee
			@employee = Employee.find(params[:id])
		end

		def set_role
			check_role("employee")
		end
end
