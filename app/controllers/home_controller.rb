class HomeController < ApplicationController
  layout :set_layout
  before_action :authenticate_user!, only: [:dashboard]
  # before_action :set_organization
  before_action :check_customer, only: [:dashboard]

  def index
    hash = params[:area]
    if hash.present?
      @organizations = Organization.search(hash[:area_id]).paginate(page: params[:page], per_page: 10)  
    else
      @organizations = Organization.paginate(page: params[:page], per_page: 10)
    end
    
  end

  def dashboard
  end

  private
  	def set_organization
      	if !current_user.is_super_admin
        	if current_user.organization.nil? && current_user.is_admin
          		redirect_to new_organization_path
        	elsif current_user.organization.active == 0
          		destroy_user_session_path
        	end
      	end
	  end

    def set_layout
      case action_name
      when "dashboard"
        "dashboard"
      else 
        "application"
      end
    end
end
