class DistrictsController < ApplicationController
	layout "dashboard"
	
	respond_to :html, :js
	before_action :authenticate_user!
	before_action :check_customer
	before_action :set_super_admin

	def index
		@districts 	= District.paginate(page: params[:page], per_page: 3)
		@district  	= District.new
	end

	def show
		@district 	= District.find(params[:id])
	end

	def new
		@district 	= District.new
	end

	def create
		@districts 	= District.all
		@district  	= District.create(district_params)
		flash[:success] = "District added successfully"
	end

	def edit
		@district 	= District.find(params[:id])
	end

	def update
		@districts 	= District.all
		@district 	= District.find(params[:id])
		@district.update_attributes(district_params)
	end 

	def delete
		@district 	= District.find(params[:district_id])
	end

	def destroy
		@district 	= District.all
		@district 	= District.find(params[:id])
		@district.destroy
	end

	private
		def district_params
			params.require(:district).permit(:name)
		end
end
