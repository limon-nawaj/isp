class SalaryInvoicesController < ApplicationController
	layout "dashboard"

	before_action :authenticate_user!
	before_action :check_customer
	before_action :set_role
	
	def index
		@employees = current_organization.employees.where(status: 1)
		
		if params[:month_year].present?
			hash = params[:month_year]
			@d 	= Date.new(hash['(1i)'].to_i, hash['(2i)'].to_i, hash['(3i)'].to_i)
			@date = Date.parse(@d.to_s)
			@my = Date.parse(@d.to_s).strftime("%m%Y")
		else
			@date = Time.now
			@my = Time.now.strftime("%m%Y")
		end
	end

	private
		def set_role
		    check_role("accountant")
		end
end
