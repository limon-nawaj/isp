class CustomersController < ApplicationController
	layout :set_layout
	before_action :authenticate_user!
	# before_action :check_customer, only: [:index, :new, :create]	
	before_action :set_customer, only: [:edit, :update, :active, :show, :basic_info, :address, :login_info]
	before_action :set_role, only: [:index, :new, :create, :active]

	def index
		@current_customers = current_organization.customers.current_customer_search(params[:current_customer_search])
		@x_customers = current_organization.customers.x_customer_search(params[:x_customer_search])
		@all_customers = current_organization.customers.all_customer_search(params[:all_customer_search])
		@icare = Icare.new
	end

	def new
		@customer = Customer.new
		@customers = current_organization.customers.order(id: :desc).limit(15)
		@customer.build_user unless @customer.user
		@customer.build_wallet unless @customer.wallet
	end

	def create
		@customer = Customer.new(customer_params)
		if @customer.save
			flash[:success] = "Customer added successfully"
			redirect_to new_customer_path
		else
			@customers = current_organization.customers.order(id: :desc).limit(15)
			render "new"
		end
	end

	def edit
	end

	def show
	end

	def basic_info
	end

	def address
	end

	def login_info
	end

	def update
		@customer.update_attributes(customer_params)
	end

	def active
		@customer.update_attributes(params.require(:customer).permit(:active))
	end

	private 
		def customer_params
			params.require(:customer).permit(:first_name, :last_name, :email, :mobile, 
				:batch_number, :organization_id, :zone_id, :package_id, :house, :block, :location_specification, 
				:type, :join_date, :active, :ip_address, :mac_address,
				user_attributes: [:id, :organization_id, :username, :password, :password_confirmation, :role, :image], wallet_attributes: [:amount])
		end

		# def update_customer_params
		# 	params.require(:customer).permit(:first_name, :last_name, :email, :mobile, 
		# 		:batch_number, :organization_id, :zone_id, :package_id, :house, :block, :location_specification, :type, :join_date, :active)
		# end

		def set_customer
			if current_user.is_customer
				@customer = current_user.meta
			else
				@customer = Customer.find(params[:id])	
			end
		end

		def set_layout
			if current_user.is_customer
				"application"
			else
				"dashboard"
			end
		end

		def set_role
		    check_role("customer")
		end
end
