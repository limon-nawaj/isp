class UsersController < ApplicationController
  layout :set_layout
  respond_to :html, :js
  before_action :authenticate_user!
  # before_action :check_customer
  # before_action :set_super_admin

  skip_before_filter :verify_authenticity_token, :only => :upload
  def admin
  	@admins = User.where(role: "admin")
  end

  def edit
  	@user = User.find(params[:id])
  end

  def update
  	@user = User.find(params[:id])
  	if @user.update_attributes(params.require(:user).permit(:password, :password_confirmation, :image))
  		flash[:success] = "Successfully"
  		redirect_to edit_user_path(@user)	
  	else
  		render 'edit'
  	end
  	
  end

  def upload
  	@user = User.find(params[:id])
  	@user.update_attributes(params.require(:user).permit(:image))
  	redirect_to edit_user_path(@user)
  end

  def customer_upload
    current_user.update_attributes(params.require(:user).permit(:image))
    @customer = Customer.find(current_user.meta_id)
    redirect_to edit_customer_path(@customer)
  end

  def employee_upload
    current_user.update_attributes(params.require(:user).permit(:image))
    @employee = Employee.find(current_user.meta_id)
    redirect_to edit_employee_path(@employee)
  end

  private
    def set_layout
      if current_user.is_customer
        "application"
      else
        "dashboard"
      end
    end
  
end
