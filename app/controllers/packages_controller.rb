class PackagesController < ApplicationController
	layout "dashboard"
	
	respond_to 	  :html, :js
	before_action :authenticate_user!
	before_action :check_customer
	before_action :set_role #office admin and admin
	before_action :set_package, only: [:edit, :update, :destroy]

	def index
		@packages = current_organization.packages.order(name: :asc)
		@package  = Package.new
	end

	def create
		@packages = current_organization.packages.order(name: :asc)
		@package = Package.create(package_params)
		flash[:success] = "Package created successfully"
	end

	def edit
	end

	def update
		@package.update_attributes(package_params)
		flash[:success] = "Package updated successfully"
	end

	def destroy
		@package.update_attributes(params.require(:package).permit(:active))
	end

	private
		def package_params
			params.require(:package).permit(:organization_id, :name, :price, :data_speed, :vat, :description, :active)
		end

		def set_package
			@package = Package.find(params[:id])
			@packages = current_organization.packages.order(name: :asc)		
		end

		def set_role 
		    check_role("office_admin")
		end
end
