class ConnectionsController < ApplicationController
	layout "dashboard", only: [:index]
	respond_to :html, :js
	before_filter :find_organization

	def index
		@connections = current_organization.connections.paginate(page: params[:page], per_page: 30)
	end

	def new
		@connection = Connection.new
	end

	def create
		@connection = Connection.new(connection_params)
		if @connection.save
			flash[:success] = "Connection requrest send successfully"
			redirect_to root_path
		else
			render 'new'
		end
	end

	def update
		@connections = current_organization.connections.order(created_at: :desc).paginate(page: params[:page], per_page: 30)
		@connection = Connection.find(params[:id])
		@connection.update_attributes(connection_params)
	end

	private

		def connection_params
			params.require(:connection).permit(:first_name, :last_name, :mobile, :email, :organization_id, :package_id, :zone_id, :house, :block, :location_specification, :status)
		end

		def find_organization
			@organization = Organization.find(params[:organization_id])
		end
end
