class OperatorsController < ApplicationController
	layout "dashboard"
	before_action :authenticate_user!
	before_action :check_customer
	before_action :set_role

	def index
		@operators = current_employee.operators
	end

	def update
		@operator = Operator.find(params[:id])
		@operators = current_employee.operators
		@icare = Operator.find(params[:id]).icare
		@icare.update_attributes(params.require(:icare).permit(:feedback, :status, :extension, :extension_minute, :reason_of_extention, :extension_hour))
	end

	private 
		def set_role
		    check_role("operator")
		end
end
