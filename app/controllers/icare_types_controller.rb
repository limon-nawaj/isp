class IcareTypesController < ApplicationController
	layout "dashboard"
	respond_to :html, :js
	before_action :authenticate_user!
	before_action :check_customer
	before_action :set_role
	before_action :set_icare_type, only: [:show, :edit, :update]

	def index
		@icare_types = current_organization.icare_types
		@icare_type = IcareType.new
	end

	def show
	end

	def create
		@icare_types = current_organization.icare_types
		@icare_type = IcareType.create(icare_type_params)
		flash[:success] = "Zone created successfully"
	end

	def show
	end

	def edit
	end

	def update
		@icare_types = current_organization.icare_types
		@icare_type.update_attributes(icare_type_params)
	end

	private 
		def icare_type_params
			params.require(:icare_type).permit(:organization_id, :name, :required_time, :active)
		end

		def set_icare_type
			@icare_type = IcareType.find(params[:id])
		end

		
		def set_role
		    check_role("office_admin")
		end
end
