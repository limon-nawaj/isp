class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper

  def notification
    @operator = Operator.all
  end
  
  private

    # def after_sign_in_path_for(resource)
      
    #   if current_user.is_super_admin
    #     dashboard_path
    #   elsif current_user.is_customer
    #     @customer = Customer.find(current_user.meta_id)
    #     customer_path(@customer)
    #   else


    #     @user_organization = current_user.organization    
    #     if !@user_organization.nil? && @user_organization.status==1
    #       dashboard_path
    #     elsif !@user_organization.nil? && @user_organization.status==0
    #       destroy_user_session_path
    #       current_user.destroy
    #     elsif @user_organization.nil? && current_user.is_admin 
    #       new_organization_path
    #     else
    #       destroy_user_session_path
    #       current_user.destroy
    #     end      
    #   end
    # end

  
# It will check if the current user is super admin and it used in controller as before_action
  def set_super_admin
    if !current_user.is_super_admin
      redirect_to dashboard_path
    end
  end

# It'll allow user to access only by admin
  def set_admin
    if !current_user.is_admin
      redirect_to dashboard_path
    end
  end

# it'll make available organization through the whole application
  def current_organization
    current_user.organization
  end

  def check_customer
    if current_user.is_customer
      @customer = Customer.find(current_user.meta_id)
      redirect_to customer_path(@customer)
    end
  end

# check role for each controller
  def check_role(role_type)
    if current_user.is_employee
    # employee start
      case role_type
      when "employee" 
        if !current_employee.role.employee
          redirect_to dashboard_path
        end
      when "bill"
        if !current_employee.role.bill
          redirect_to dashboard_path
        end
      when "office_admin"
        if !current_employee.role.office_admin
          redirect_to dashboard_path
        end
      when "customer"
        if !current_employee.role.customer
          redirect_to dashboard_path
        end
      when "accountant"
        if !current_employee.role.accountant
          redirect_to dashboard_path
        end
      end
    # employee end
    else
      if !current_user.is_admin
        redirect_to dashboard_path
      end
    end
  end
end
