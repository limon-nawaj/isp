class ZonesController < ApplicationController
	layout "dashboard"
	respond_to :html, :js
	before_action :authenticate_user!
	before_action :check_customer
	before_action :set_role
	before_action :set_zone, only: [:show, :edit, :update]

	def index
		@zones = current_organization.zones.paginate(page: params[:page], per_page: 10)
		@zone = Zone.new
	end

	def show
	end

	def create
		@zones = current_organization.zones.paginate(page: params[:page], per_page: 10)
		@zone = Zone.create(zone_params)
		flash[:success] = "Zone created successfully"
	end

	def edit
	end

	def update
		@zones = current_organization.zones.paginate(page: params[:page], per_page: 10)
		@zone.update_attributes(zone_params)
	end

	private
		def set_zone
			@zone = Zone.find(params[:id])
		end

		def zone_params
			params.require(:zone).permit(:organization_id, :name)
		end

		def set_role
		    check_role("office_admin")
		end
end
