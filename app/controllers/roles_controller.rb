class RolesController < ApplicationController
	layout "dashboard"
	
	respond_to :html, :js
	before_action :authenticate_user!
	before_action :check_customer
	# before_action :set_admin
	before_action :set_role, only: [:edit, :show, :update]
	before_action :set_roles
	before_action :set_office_admin
	
	def index
		@role = Role.new
	end

	def show
	end

	def create
		@role = Role.create(role_params)
	end

	def edit
	end

	def update
		@role.update_attributes(role_params)
		redirect_to roles_path
	end

	private
		def set_role
			@role = Role.find(params[:id])
		end

		def set_roles
			@roles = current_organization.roles.paginate(page: params[:page], per_page: 10)
		end

		def role_params 
			params.require(:role).permit(:organization_id, :name, :office_admin, :employee, :accountant, :service_monitor, :customer, :bill, :operator)
		end

		def set_office_admin
			# if current_user.is_employee
			# 	if !current_employee.role.office_admin
			#       redirect_to dashboard_path
			#     end
			# else
			# 	if !current_user.is_admin
			# 		redirect_to dashboard_path
			# 	end
			# end
		    check_role("office_admin")
		end
end