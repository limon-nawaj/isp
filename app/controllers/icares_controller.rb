class IcaresController < ApplicationController
	# 1 = pending, 2 = on process, 3 = feedback given/completed, 4= time extension, 5 = problem still exist, 10 = finish or accepted
	layout :set_layout
	before_action :authenticate_user!
	before_action :check_customer, only: [:update, :assign_task, :services]
	before_action :set_icare, only: [:edit, :update, :active, :show, :destroy, :assign_task]
	before_action :icares, only: [:index, :admin_index, :update, :create, :destroy, :assign_task]
	before_action :set_role, only: [:admin_index, :update, :assign_task]
	# before_action :set_operator, only: [:services]

	def index
		@icare  = Icare.new
		@icare.operators.build
		@roles = current_organization.roles
	end

	def admin_index
		@icare  = Icare.new
		@icare.operators.build
		@roles = current_organization.roles
	end

	def show
		
	end

	def new

	end

	def create
		# if current_user.is_customer
		# 	@icare = Icare.create(icare_params)
		# else
		# 	@batch_numerb = params[:customer_id].to_i
		# 	params[:icare][:customer_id] = Customer.find_by_batch_number(@batch_numerb).first.id
		# 	@icare = Icare.new(icare_params)
		# 	if @icare.save
		# 		flash[:success] = "Icare is added successfull"
		# 		redirect_to new_icare_path
		# 	else
		# 		render 'new'
		# 	end
		# end

		@icare = Icare.new(icare_params)
		if @icare.save
			flash[:success] = "Icare is added successfull"
			if !current_user.is_customer
				redirect_to admin_index_icares_path
			end
		else
			render 'new'
		end
	end

	def update
		@icare.update_attributes(icare_params)
	end

	def assign_task
		@employees = Employee.find(params[:employee_ids])
		
		if @employees.present?
			@icare.update_attributes(icare_params)
			@operators = Operator.where(icare_id: @icare.id)
			if @operators.present?
				@operators.each do |operator|
					operator.destroy
				end
			end

			@employees.each do |employee|
				Operator.create(icare_id: @icare.id, employee_id: employee.id)
			end
		end
	end

	def destroy
		@icare.destroy
	end

	private
		def icare_params
			params.require(:icare).permit(:organization_id, :customer_id, :icare_type_id, :problem, :feedback, 
				:reciever_id, :resoponsible_id, :required_time, :extension, :not_fixed, :status, :hour, :minute)
		end

		def set_icare
			@icare = Icare.find(params[:id])
		end

		def set_layout
			if current_user.is_customer
				"application"
			else
				"dashboard"
			end
		end

		def icares
			if current_user.is_customer
				@customer = Customer.find(current_user.meta_id)
				@icares = @customer.icares.order(created_at: :desc).paginate(page: params[:page], per_page: 30)
			else
				@icares = current_organization.icares.order(created_at: :desc).paginate(page: params[:page], per_page: 30)
			end
		end

		def set_role
		    check_role("icare")
		end
end
