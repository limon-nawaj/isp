class OrganizationsController < ApplicationController
	layout "dashboard"
	before_action :authenticate_user!
	before_action :check_customer
	# before_action :set_organization, except: [:new, :create]  
	
	def index
		@organizations = Organization.order(:name).paginate(page: params[:page], per_page: 20)
	end

	def new
		@organization = Organization.new
		@organization.users.build
	end

	def create
		@organization = Organization.new(organization_params)
		if @organization.save
			# @user = User.find(current_user.id)
			# @user.update_attributes!(organization_id: @organization.id, password: "limon", password_confirmation: "limon", 
			# 			username: @user.username, first_name: @user.first_name, email: @user.email)
			redirect_to organizations_path
			flash[:success] = "Organization added successfully"
		else
			render 'new'			
		end
	end

	def edit
		@organization = Organization.find(params[:id])
	end

	def update
		@organization = Organization.find(params[:id])
		if @organization.update_attributes(organization_params)
			redirect_to edit_organization_path(@organization)
			flash[:success] = "Organization updated successfully"
		else
			render 'edit'
		end
	end

	def logo
		@organization = Organization.find(params[:id])
		@organization.update_attributes(params.require(:organization).permit(:image))
		redirect_to root_path
	end

	private

		def organization_params
			params.require(:organization).permit(
				:name, :mobile, :email, :district_id, :area_id, 
				:house, :block, :specific_location, :about, users_attributes: [:id, :username, :password, :password_confirmation, :role])
		end

		# def set_layout
		# 	case action_name
		# 	when "new", "create"
		# 		"authentications"
		# 	else
		# 		"dashboard"
		# 	end
		# end
end
