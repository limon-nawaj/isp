class SessionsController < ApplicationController
	layout "authentications"

	def new
  	end

  	def create
  		user = User.find_by(username: params[:session][:username].downcase)
      if user && user.authenticate(params[:session][:password])
        if user.active
          sign_in user
          if user.is_customer
            # sign_in user
            # @customer = Customer.where(meta_id: user.meta_id).where(meta_type: "Customer")
            redirect_back_or customer_path(current_user.meta)
          else
            redirect_back_or dashboard_path
          end
        else
          sign_out
          flash[:notice] = "You are no longer authorized to access please communicate with admin"
          redirect_to root_path
        end
      else
        flash.now[:error] = 'Invalid username/password combination'
        render 'new'
      end
        


       #  if user && user.authenticate(params[:session][:password])
       #  	sign_in user
       #  	redirect_back_or dashboard_path
      	# else
       #  	flash.now[:error] = 'Invalid username/password combination'
       #  	render 'new'
      	# end
  	end



  	def destroy
  		sign_out
      redirect_to root_path
  	end
end
