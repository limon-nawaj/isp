class AreasController < ApplicationController
	layout "dashboard"
	before_action :authenticate_user!
	before_action :set_super_admin
	before_action :check_customer

	
	respond_to :html, :js
	before_action :set_district

	def index
		@areas = @district.areas
	end

	def create
		Area.create(area_params)
		flash[:success] = "Area created successfully"
	end

	private
		def set_district
			@area = Area.new
			@district = District.find(params[:district_id])
		end

		def area_params
			params.require(:area).permit(:name, :district_id)
		end
end
