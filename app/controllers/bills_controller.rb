class BillsController < ApplicationController
	layout "dashboard"
	respond_to :html, :js

	before_action :authenticate_user!
	before_action :check_customer
	before_action :set_bills, only: [:index, :update, :destroy]
	before_action :set_role #bill, admin

	def index
	end

	def create
		@customers = Customer.find(params[:customer_ids])
		if @customers.present?
			@customers.each do |customer|
				@wallet = customer.wallet.amount
				@price = customer.package.f_price

				if @wallet.to_f>=@price.to_f
					Bill.create(organization_id: current_organization.id, customer_id: customer.id, package_id: customer.package_id, 
							zone_id: customer.zone_id, month_year: Date.parse(params[:month_year].to_s), debit: customer.package.f_price, 
							credit: @price.to_f)

					customer.wallet.update_attributes(amount: (@wallet.to_f-@price.to_f))
				elsif @wallet.to_f<@price.to_f 
					if @wallet.to_f>0
						Bill.create(organization_id: current_organization.id, customer_id: customer.id, package_id: customer.package_id, 
							zone_id: customer.zone_id, month_year: Date.parse(params[:month_year].to_s), debit: customer.package.f_price, 
							credit: @wallet.to_f)
					else
						Bill.create(organization_id: current_organization.id, customer_id: customer.id, package_id: customer.package_id, 
							zone_id: customer.zone_id, month_year: Date.parse(params[:month_year].to_s), debit: customer.package.f_price, 
							credit: 0)
					end
					customer.wallet.update_attributes(amount: @wallet.to_f-@price.to_f)
				end
			end	
			redirect_to bills_path
		end
	end

	def update
		@bill = Bill.find(params[:id])

		@wallet = @bill.customer.wallet.amount
		@bill.update_attributes(credit: -@wallet.to_f)
		@bill.customer.wallet.update_attributes(amount: 0)
	end

	def custom_paid
		@bill = Bill.find(params[:id])
		@previous_credit = @bill.credit #take the prevoice credit and add with the wallet
		@bill.update_attributes(params.require(:bill).permit(:credit)) 
		@wallet = -(@previous_credit.to_f-@bill.customer.wallet.amount.to_f)+@bill.credit.to_f
		@bill.customer.wallet.update_attributes(amount: @wallet)
	end

	def destroy
		@bill = Bill.find(params[:id])
		@wallet = @bill.customer.wallet.amount.to_f+@bill.debit.to_f
		@bill.customer.wallet.update_attributes(amount: @wallet)
		@bill.destroy
	end

	private
		def bill_params
			params.require(:bill).permit(:organization_id, :customer_id, :date, :credit, :debit)
		end


		def set_bills
			if params[:month_year].present?
				hash = params[:month_year]
				@d 	= Date.new(hash['(1i)'].to_i, hash['(2i)'].to_i, hash['(3i)'].to_i)
				@my = Date.parse(@d.to_s).strftime("%m%Y")
				@bills = current_organization.bills.search(@my, params[:batch_number], params[:package_id], params[:zone_id])
				if !@bills.nil?
					@bills = @bills.paginate(page: params[:page], per_page: 50)
				end
			else
				@my = Time.now.strftime("%m%Y")
				@bills = current_organization.bills.search(@my, params[:batch_number], params[:package_id], params[:zone_id]).paginate(page: params[:page], per_page: 50)
			end
		end

		# def paid(wallet, paid)
		# 	wallet.to_f+paid.to_f
		# end

		def set_role
		    check_role("bill")
		end
end
