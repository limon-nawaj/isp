class SalariesController < ApplicationController
	layout "dashboard"
	respond_to :html, :js

	before_action :authenticate_user!
	before_action :check_customer
	before_action :set_salaries, only: [:index, :update, :destroy]
	before_action :set_role
	
	def index
	end

	def create
		@employees = Employee.find(params[:employee_ids])
		if @employees.present?
			@employees.each do |employee|
				@wallet = employee.wallet.amount
				@salary = employee.salary

				if @wallet.to_f>=@salary.to_f
					Salary.create(organization_id: current_organization.id, employee_id: employee.id,  
							month_year: Date.parse(params[:month_year].to_s), debit: employee.salary, 
							credit: @salary.to_f)

					employee.wallet.update_attributes(amount: (@salary.to_f-@wallet.to_f))
				elsif @wallet.to_f<@salary.to_f 
					if @wallet.to_f>0
						Salary.create(organization_id: current_organization.id, employee_id: employee.id,  
							month_year: Date.parse(params[:month_year].to_s), debit: employee.salary, 
							credit: @wallet.to_f)
					else
						Salary.create(organization_id: current_organization.id, employee_id: employee.id,  
							month_year: Date.parse(params[:month_year].to_s), debit: employee.salary, 
							credit: 0)
					end
					employee.wallet.update_attributes(amount: @salary.to_f-@wallet.to_f)
				end
			end	
			redirect_to salaries_path
		end
	end

	def update
		@bill = Salary.find(params[:id])

		@wallet = @bill.employee.wallet.amount
		@bill.update_attributes(credit: @wallet.to_f)
		@bill.employee.wallet.update_attributes(amount: 0)
	end

	def custom_paid
		@salary = Salary.find(params[:id])
		@previous_credit = @salary.credit #take the prevoice credit and add with the wallet
		@salary.update_attributes(params.require(:salary).permit(:credit)) 
		@wallet = (@salary.employee.wallet.amount.to_f+@previous_credit.to_f)-@salary.credit.to_f
		@salary.employee.wallet.update_attributes(amount: @wallet)
	end

	def destroy
		@salary = Salary.find(params[:id])
		@wallet = @salary.employee.wallet.amount.to_f-@salary.debit.to_f
		@salary.employee.wallet.update_attributes(amount: @wallet)
		@salary.destroy
	end

	private
		def salary_params
			params.require(:salary).permit(:organization_id, :customer_id, :date, :credit, :debit)
		end


		def set_salaries
			if params[:month_year].present?
				hash = params[:month_year]
				@d 	= Date.new(hash['(1i)'].to_i, hash['(2i)'].to_i, hash['(3i)'].to_i)
				@my = Date.parse(@d.to_s).strftime("%m%Y")
				@salaries = current_organization.salaries.search(@my, params[:batch_number])
				if !@salaries.nil?
					@salaries = @salaries.paginate(page: params[:page], per_page: 50)
				end
			else
				@my = Time.now.strftime("%m%Y")
				@salaries = current_organization.salaries.search(@my, params[:batch_number]).paginate(page: params[:page], per_page: 50)
			end
		end

		# def paid(wallet, paid)
		# 	wallet.to_f+paid.to_f
		# end

		def set_role
		    check_role("accountant")
		end
end
