module NotificationHelper
	


	# Operator notication
	def operator_panding_service
		number_of_panding = 0
		current_employee.operators.each do |operator|
			if operator.icare.status != 10
				number_of_panding = number_of_panding+1
			end
		end
		return number_of_panding
	end

	def operator_service_completed
		number_of_panding = 0
		current_employee.operators.each do |operator|
			if operator.icare.status == 10
				number_of_panding = number_of_panding+1
			end
		end
		return number_of_panding
	end

	def operator_overdue
		number_of_panding = 0
		current_employee.operators.each do |operator|
			if operator.icare.created_at <= 2.week.ago && operator.icare.status != 10 
				number_of_panding = number_of_panding+1
			end
		end
		return number_of_panding
	end

	# service monitoring
	def monitor_pending
		number_of_panding = 0
		current_organization.icares.each do |icare|
			if icare.status == 1
				number_of_panding =+ 1
			end
		end
		return number_of_panding
	end
end
