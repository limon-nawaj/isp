module ApplicationHelper
	def current_organization
	    current_user.organization
	end

	def notification_operator
		Operator.all
	end
end
