class CreateSalaries < ActiveRecord::Migration
  def change
    create_table :salaries do |t|
     	t.integer  :organization_id
    	t.integer  :employee_id
    	t.date 	   :month_year
    	t.float 	 :credit, default: 0
    	t.float 	 :debit, default: 0
	    t.timestamps
    end
  end
end
