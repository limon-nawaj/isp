class CreateIcares < ActiveRecord::Migration
  def change
    create_table :icares do |t|
      t.integer 	:organization_id 
      t.integer		:customer_id
      t.integer 	:icare_type_id
      t.string 		:problem
      t.string		:feedback
      t.integer 	:reciever_id
      # t.integer 	:resoponsible_id
      t.integer   :hour,              default: 0
      t.integer   :minute,            default: 0
      t.string    :reason_of_extention
      t.integer 	:extension,         default: 0 #increase by extension
      t.integer   :extension_hour
      t.integer   :extension_minute
      # t.integer   :not_fixed, default: 0 #increase if the problem isn't fixed
      t.integer		:status
      t.timestamps
    end
  end
end
