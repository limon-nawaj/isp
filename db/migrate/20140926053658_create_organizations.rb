class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table  :organizations do |t|
      t.string    :name
      t.string    :mobile
      t.string    :email
      t.integer   :district_id
      t.integer   :area_id
      t.string    :block
      t.string    :house
      t.string    :specific_location
      t.string    :about
      t.string    :services
      t.integer   :status, default: 1
      t.timestamps
    end
  end
end
