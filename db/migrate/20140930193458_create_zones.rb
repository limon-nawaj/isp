class CreateZones < ActiveRecord::Migration
  def change
    create_table :zones do |t|
      t.string :name
      t.integer :organization_id

      t.timestamps
    end
  end
end
