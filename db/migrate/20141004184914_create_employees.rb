class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      # general field
      t.string  :first_name
      t.string  :last_name
      t.string  :mobile
      t.string  :email
      t.integer :role_id
      
      t.integer :batch_number
      t.string :address
      t.integer :organization_id
      t.string :basic_salary
      t.string :additional_salary
      t.integer :status, default: 1
      t.date :join_date
      t.timestamps
    end
  end
end
