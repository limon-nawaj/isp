class CreateOperators < ActiveRecord::Migration
  def change
    create_table :operators do |t|
      t.integer :icare_id
      t.integer :employee_id
      t.boolean :pass
      t.timestamps
    end
  end
end
