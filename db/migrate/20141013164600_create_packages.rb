class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.integer :organization_id
      t.string  :name
      t.float   :price
      t.string  :data_speed
      t.float   :vat,      default: 0.0
      t.string  :description
      t.boolean :active, default: true
      t.timestamps
    end
  end
end
