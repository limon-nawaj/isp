class CreateBills < ActiveRecord::Migration
  def change
    create_table :bills do |t|
    	t.integer 	:organization_id
    	t.integer 	:customer_id
      t.integer   :taken_by
      t.integer   :package_id
      t.integer   :zone_id
    	t.date 	    :month_year
    	t.float 	  :credit, default: 0
    	t.float 	  :debit, default: 0
	    t.timestamps
    end
  end
end
