class CreateConnections < ActiveRecord::Migration
  def change
    create_table :connections do |t|
      t.string  :first_name
      t.string  :last_name
      t.string  :mobile
      t.string  :email
      
      t.integer :organization_id
      t.integer :package_id
      t.integer :zone_id

      t.string  :house
      t.string  :block
      t.string  :location_specification
      t.integer :status, default: 0
      t.timestamps
    end
  end
end
