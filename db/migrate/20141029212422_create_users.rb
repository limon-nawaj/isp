class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      ## Database authenticatable
      t.string :username, null: false, default: ""
      t.string :password_digest
      # general field
      t.string :first_name
      t.string :last_name
      t.string :mobile
      t.string :email
      t.integer :organization_id
      t.string :role
      t.string :remember_token

      # polymorphic information
      t.integer :meta_id
      t.string :meta_type
      t.boolean :active, default: true
      
      t.timestamps	
      t.timestamps
    end
    add_index :users, :username,             unique: true
    add_index :users, :remember_token
  end
end
