class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.integer  :organization_id
      t.string 	 :name
      t.boolean  :office_admin,     default: false
      t.boolean  :employee,         default: false
      t.boolean  :accountant,       default: false
      t.boolean  :service_monitor,  default: false
      t.boolean  :customer,         default: false
      t.boolean  :bill,             default: false
      t.boolean  :operator,         default: false
      t.timestamps
    end
  end
end
