class CreateAreas < ActiveRecord::Migration
  def change
    create_table :areas do |t|
      t.integer :district_id
      t.string  :name
      t.string  :status
      t.timestamps
    end
  end
end
