class CreateIcareTypes < ActiveRecord::Migration
  def change
    create_table :icare_types do |t|
      t.integer :organization_id
      t.string 	:name
      t.time    :required_time
      t.boolean :active, default: true	
      t.timestamps
    end
  end
end
