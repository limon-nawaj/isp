class CreateUserOrganizations < ActiveRecord::Migration
  def change
    create_table :user_organizations do |t|
      t.integer :user_id
      t.integer :organization_id
      t.integer :active, default: 1
      t.timestamps
    end
  end
end
