class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      # general field
      t.string  :first_name
      t.string  :last_name
      t.string  :mobile
      t.string  :email
      t.integer :batch_number
      
      t.integer :organization_id
      t.integer :package_id
      t.integer :zone_id

      t.string  :ip_address
      t.string  :mac_address

      t.string  :house
      t.string  :block
      t.string  :location_specification
      t.string  :type
      t.date    :join_date
      t.boolean :active, default: true
      t.timestamps
    end
  end
end
