class CreateWallets < ActiveRecord::Migration
  def change
    create_table :wallets do |t|
      t.integer :fee_id
      t.string :fee_type
      t.float 	:amount
      t.timestamps
    end
  end
end
