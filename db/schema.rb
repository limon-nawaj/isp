# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141118181924) do

  create_table "areas", force: true do |t|
    t.integer  "district_id"
    t.string   "name"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bills", force: true do |t|
    t.integer  "organization_id"
    t.integer  "customer_id"
    t.integer  "taken_by"
    t.integer  "package_id"
    t.integer  "zone_id"
    t.date     "month_year"
    t.float    "credit",          default: 0.0
    t.float    "debit",           default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "connections", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "mobile"
    t.string   "email"
    t.integer  "organization_id"
    t.integer  "package_id"
    t.integer  "zone_id"
    t.string   "house"
    t.string   "block"
    t.string   "location_specification"
    t.integer  "status",                 default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customers", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "mobile"
    t.string   "email"
    t.integer  "batch_number"
    t.integer  "organization_id"
    t.integer  "package_id"
    t.integer  "zone_id"
    t.string   "ip_address"
    t.string   "mac_address"
    t.string   "house"
    t.string   "block"
    t.string   "location_specification"
    t.string   "type"
    t.date     "join_date"
    t.boolean  "active",                 default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "districts", force: true do |t|
    t.string   "name"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "employees", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "mobile"
    t.string   "email"
    t.integer  "role_id"
    t.integer  "batch_number"
    t.string   "address"
    t.integer  "organization_id"
    t.string   "basic_salary"
    t.string   "additional_salary"
    t.integer  "status",            default: 1
    t.date     "join_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "icare_types", force: true do |t|
    t.integer  "organization_id"
    t.string   "name"
    t.time     "required_time"
    t.boolean  "active",          default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "icares", force: true do |t|
    t.integer  "organization_id"
    t.integer  "customer_id"
    t.integer  "icare_type_id"
    t.string   "problem"
    t.string   "feedback"
    t.integer  "reciever_id"
    t.integer  "hour"
    t.integer  "minute"
    t.string   "reason_of_extention"
    t.integer  "extension",           default: 0
    t.integer  "extension_hour"
    t.integer  "extension_minute"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "operators", force: true do |t|
    t.integer  "icare_id"
    t.integer  "employee_id"
    t.boolean  "pass"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "organizations", force: true do |t|
    t.string   "name"
    t.string   "mobile"
    t.string   "email"
    t.integer  "district_id"
    t.integer  "area_id"
    t.string   "block"
    t.string   "house"
    t.string   "specific_location"
    t.string   "about"
    t.string   "services"
    t.integer  "status",            default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
  end

  create_table "packages", force: true do |t|
    t.integer  "organization_id"
    t.string   "name"
    t.float    "price"
    t.string   "data_speed"
    t.float    "vat",             default: 0.0
    t.string   "description"
    t.boolean  "active",          default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: true do |t|
    t.integer  "organization_id"
    t.string   "name"
    t.boolean  "office_admin",    default: false
    t.boolean  "employee",        default: false
    t.boolean  "accountant",      default: false
    t.boolean  "service_monitor", default: false
    t.boolean  "customer",        default: false
    t.boolean  "bill",            default: false
    t.boolean  "operator",        default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "salaries", force: true do |t|
    t.integer  "organization_id"
    t.integer  "employee_id"
    t.date     "month_year"
    t.float    "credit",          default: 0.0
    t.float    "debit",           default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_organizations", force: true do |t|
    t.integer  "user_id"
    t.integer  "organization_id"
    t.integer  "active",          default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username",        default: "",   null: false
    t.string   "password_digest"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "mobile"
    t.string   "email"
    t.integer  "organization_id"
    t.string   "role"
    t.string   "remember_token"
    t.integer  "meta_id"
    t.string   "meta_type"
    t.boolean  "active",          default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
  end

  add_index "users", ["remember_token"], name: "index_users_on_remember_token"
  add_index "users", ["username"], name: "index_users_on_username", unique: true

  create_table "wallets", force: true do |t|
    t.integer  "fee_id"
    t.string   "fee_type"
    t.float    "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "zones", force: true do |t|
    t.string   "name"
    t.integer  "organization_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
